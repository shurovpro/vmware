$vm = Read-Host -Prompt 'Input VM name'
$spec = New-Object VMware.Vim.VirtualMachineConfigSpec
$spec.tools = New-Object VMWare.Vim.ToolsConfigInfo
$spec.tools.syncTimeWithHost = $false
New-AdvancedSetting -Entity $vm -Name time.synchronize.continue -Value 0 -Confirm:$false -Force:$true
New-AdvancedSetting -Entity $vm -Name time.synchronize.restore -Value 0 -Confirm:$false -Force:$true
New-AdvancedSetting -Entity $vm -Name time.synchronize.resume.disk -Value 0 -Confirm:$false -Force:$true
New-AdvancedSetting -Entity $vm -Name time.synchronize.shrink -Value 0 -Confirm:$false -Force:$true
New-AdvancedSetting -Entity $vm -Name time.synchronize.tools.startup -Value 0 -Confirm:$false -Force:$true
New-AdvancedSetting -Entity $vm -Name time.synchronize.tools.enable -Value 0 -Confirm:$false -Force:$true
New-AdvancedSetting -Entity $vm -Name time.synchronize.resume.host -Value 0 -Confirm:$false -Force:$true
$vm = get-vm $VM
$vm.Extensiondata.ReconfigVM_Task($spec)