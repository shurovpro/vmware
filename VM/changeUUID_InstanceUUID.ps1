$vm = Read-Host -Prompt 'Input VM name'
$vmUUID = Get-VM $vm | %{(Get-View $_.Id).config.instanceUuid}
$vmUUID
function ChangeInstance-UUID {
[CmdletBinding(
    ConfirmImpact = 'High',
    SupportsShouldProcess = $true
)]
    param(
        [switch]$Force
    )
    if ($Force -or $PSCmdlet.ShouldProcess($VM,"Change UUID")) {
        $date = get-date -format "ddhhmmss"
        $newinstanceUuid  = $vmUUID.remove(28) + $date
        $spec = New-Object VMware.Vim.VirtualMachineConfigSpec
        $spec.instanceUuid = $newinstanceUuid 
        $vm = get-vm $VM
        $vm.Extensiondata.ReconfigVM_Task($spec)
    }
}
ChangeInstance-UUID
for ($a=1; $a -lt 100; $a++)
{Write-Progress -Activity "Working..." -PercentComplete $a -CurrentOperation "$a% complete" -Status "Please wait."
  Start-Sleep -Milliseconds 10
}
Write-Output "VM: " $VM "New instanceUuid : " 
Get-VM $vm | %{(Get-View $_.Id).config.instanceUuid}
